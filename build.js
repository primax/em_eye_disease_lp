var fs  = require('fs')
var CleanCSS = require('clean-css')
var UglifyJS = require("uglify-js")

fs.readFile('./index.html', 'utf8', function (err,data) {
  if (err) { return console.log(err) }

  let pageSections = data.split('<!-- DELIVERY -->')
  let pageSection = pageSections[1]
  	.replace('<script src="/build/main.bundle.js"></script>', ' ')
  	.replace('<link rel="stylesheet" href="/css/main.css">', ' ')
  	.replace(/src="wcsstore\//g, 'src="/wcsstore/')
  
  let minfiedCss = new CleanCSS({}).minify( fs.readFileSync('css/main.css') )
  let cssInsert = '<style>' + minfiedCss.styles + '</style>'

  let jslibs = fs.readFileSync('build/libs.min.js')
  let minJs = UglifyJS.minify('build/main.bundle.js')
  let jsInsert = '<script>' + jslibs + minJs.code + '</script>'

  let page = cssInsert + pageSection + jsInsert
  fs.writeFileSync('build/delivery.html', page)
  console.log('Built for delivery')
})