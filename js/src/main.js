;( function () {
    
    var colorBlindQuiz = {
        
        updateCompareImages: function(){
            // Image comparisons
            $('.types_button').click(function(){
                var compare = $(this).data('vision');
                $('.types_button').removeClass('active');
                $(this).addClass('active');
                $('.compare_wrapper').hide();
                $('.compare_wrapper[data-compare=' + compare + ']').css('display', 'flex');
            })

            // $('.compare_instructions_close').click(function(){
            //     $("#instructions").addClass("d-none");
            //     $("#explore").removeClass("d-none");
            //     $("#explore").addClass("d-flex");
            //     if ($(window).width() <= 991) {
            //         $(".compare_images").css('height', '100vw');
            //     }
            // });

            $('.compare_select_item').click(function(){
                if ($(window).width() <= 991) {
                    $(".compare_images").css('height', '100vw');
                }

                // if #instructions does not have .d-none then add .d-none
                // if(!$("#instructions").hasClass("d-none")) {
                //     $("#instructions").addClass("d-none");
                // }

                // if #instructions has .d-flex then remove .d-flex and add .d-none
                if($("#instructions").hasClass("d-flex")) {
                    $("#instructions").removeClass("d-flex");
                    $("#instructions").addClass("d-none");
                }
                // if .cd-image-container has .d-none then remove .d-none
                if($(".cd-image-container").hasClass("d-none")) {
                    $(".cd-image-container").removeClass("d-none");   
                }
                let clickedBtn = this;
                let compareContainer = this.parentElement;

                let btns = $(".compare_select_item");
                Array.prototype.forEach.call(btns, function(btn) {
                    if (btn != clickedBtn) {
                        btn.className += " d-none";
                    }
                });

                if (compareContainer.classList.contains("compare_select_container_active")) {
                    compareContainer.classList.remove("compare_select_container_active");
                    resetBtns();
                }
                else {
                    compareContainer.className += " compare_select_container_active";
                }

                var image = $(this).data('select');
                var wrapper = $(this).parents('.compare_wrapper')[0];
                $(wrapper).find('.compare_select_item').removeClass('compare_active');
                $(this).addClass('compare_active');
                var originalImg = $(wrapper).find('.compare_image_original')
                var colorImg = $(wrapper).find('.compare_image_color')
                var originalImgSrc = originalImg.data($(this).data('select'));
                var colorImgSrc = colorImg.data($(this).data('select'));
                $(originalImg).attr('src', originalImgSrc);
                $(colorImg).attr('src', colorImgSrc);
            });

            $('.compare_select_go_back').click(function(){
                $(".compare_select_container").removeClass("compare_select_container_active");
                resetBtns();
            });

            $('.compare_select_doctors_view').click(function(){
                $(".compare_select_doctors_view_active").removeClass("compare_select_doctors_view_active");
                $(".has_disabled_button").removeClass("has_disabled_button");
                $(this).addClass("compare_select_doctors_view_active");
                $(this).parent(".compare_select_doctors_view_wrapper").addClass("has_disabled_button");
                //$(this).siblings(".compare_select_doctors_view").removeClass("compare_select_doctors_view_active");
                if ($(this).html() == "What is it?") {
                    $(".compare_select_item_text").removeClass("d-none");
                    $(".see_doctors_view_text").addClass("d-none");
                } else {
                    $(".compare_select_item_text").addClass("d-none");
                    $(".see_doctors_view_text").removeClass("d-none");
                }
            });

            function resetBtns() {
                let btns = $(".compare_select_item");
                Array.prototype.forEach.call(btns, function(btn) {
                    btn.classList.remove("d-none");
                });
                let doctorsViewBtns = $(".compare_select_doctors_view");
                Array.prototype.forEach.call(doctorsViewBtns, function(btn) {
                    if ($(btn).html() == "What is it?") {
                        $(btn).addClass("compare_select_doctors_view_active");
                    } else {
                        $(btn).removeClass("compare_select_doctors_view_active");
                    }
                });
                $(".cd-image-container").addClass("d-none");
                $("#instructions").addClass("d-flex");
                $("#instructions").removeClass("d-none");
                $(".cd-resize-img").removeAttr('style');
                $(".cd-handle").removeAttr('style');
                $(".compare_select_item_text").removeClass("d-none");
                $(".see_doctors_view_text").addClass("d-none");
            }
        },

        initCompareImages: function(){
            var dragging = false,
            scrolling = false,
            resizing = false;
            //cache jQuery objects
            var imageComparisonContainers = $('.cd-image-container');
            //check if the .cd-image-container is in the viewport 
            //if yes, animate it
            checkPosition(imageComparisonContainers);
            $(window).on('scroll', function(){
                if( !scrolling) {
                    scrolling =  true;
                    ( !window.requestAnimationFrame )
                        ? setTimeout(function(){checkPosition(imageComparisonContainers);}, 100)
                        : requestAnimationFrame(function(){checkPosition(imageComparisonContainers);});
                }
            });
            
            //make the .cd-handle element draggable and modify .cd-resize-img width according to its position
            imageComparisonContainers.each(function(){
                var actual = $(this);
                drags(actual.find('.cd-handle'), actual.find('.cd-resize-img'), actual, actual.find('.cd-image-label[data-type="original"]'), actual.find('.cd-image-label[data-type="modified"]'));
            });

            //upadate images label visibility
            $(window).on('resize', function(){
                if( !resizing) {
                    resizing =  true;
                    ( !window.requestAnimationFrame )
                        ? setTimeout(function(){checkLabel(imageComparisonContainers);}, 100)
                        : requestAnimationFrame(function(){checkLabel(imageComparisonContainers);});
                }
            });

            function checkPosition(container) {
                container.each(function(){
                    var actualContainer = $(this);
                    if( $(window).scrollTop() + $(window).height()*0.5 > actualContainer.offset().top) {
                        actualContainer.addClass('is-visible');
                    }
                });

                scrolling = false;
            }

            function checkLabel(container) {
                container.each(function(){
                    var actual = $(this);
                    updateLabel(actual.find('.cd-image-label[data-type="modified"]'), actual.find('.cd-resize-img'), 'left');
                    updateLabel(actual.find('.cd-image-label[data-type="original"]'), actual.find('.cd-resize-img'), 'right');
                });

                resizing = false;
            }

            //draggable funtionality - credits to http://css-tricks.com/snippets/jquery/draggable-without-jquery-ui/
            function drags(dragElement, resizeElement, container, labelContainer, labelResizeElement) {
                dragElement.on("mousedown vmousedown", function(e) {
                    dragElement.addClass('draggable');
                    resizeElement.addClass('resizable');

                    var dragWidth = dragElement.outerWidth(),
                        xPosition = dragElement.offset().left + dragWidth - e.pageX,
                        containerOffset = container.offset().left,
                        containerWidth = container.outerWidth(),
                        minLeft = containerOffset - 40,
                        maxLeft = containerOffset + containerWidth - dragWidth + 46;
                    
                    dragElement.parents().on("mousemove vmousemove", function(e) {
                        if( !dragging) {
                            dragging =  true;
                            ( !window.requestAnimationFrame )
                                ? setTimeout(function(){animateDraggedHandle(e, xPosition, dragWidth, minLeft, maxLeft, containerOffset, containerWidth, resizeElement, labelContainer, labelResizeElement);}, 100)
                                : requestAnimationFrame(function(){animateDraggedHandle(e, xPosition, dragWidth, minLeft, maxLeft, containerOffset, containerWidth, resizeElement, labelContainer, labelResizeElement);});
                        }
                    }).on("mouseup vmouseup", function(e){
                        dragElement.removeClass('draggable');
                        resizeElement.removeClass('resizable');
                    });
                    e.preventDefault();
                }).on("mouseup vmouseup", function(e) {
                    dragElement.removeClass('draggable');
                    resizeElement.removeClass('resizable');
                });
            }

            function animateDraggedHandle(e, xPosition, dragWidth, minLeft, maxLeft, containerOffset, containerWidth, resizeElement, labelContainer, labelResizeElement) {
                var leftValue = e.pageX + xPosition - dragWidth;   
                //constrain the draggable element to move inside his container
                if(leftValue < minLeft ) {
                    leftValue = minLeft;
                } else if ( leftValue > maxLeft) {
                    leftValue = maxLeft;
                }

                var widthValue = (leftValue + dragWidth/2 - containerOffset)*100/containerWidth+'%';
                
                $('.draggable').css('left', widthValue).on("mouseup vmouseup", function() {
                    $(this).removeClass('draggable');
                    resizeElement.removeClass('resizable');
                });

                $('.resizable').css('width', widthValue); 

                updateLabel(labelResizeElement, resizeElement, 'left');
                updateLabel(labelContainer, resizeElement, 'right');
                dragging =  false;
            }

            function updateLabel(label, resizeElement, position) {
                if(position == 'left') {
                    ( label.offset().left + label.outerWidth() < resizeElement.offset().left + resizeElement.outerWidth() ) ? label.removeClass('is-hidden') : label.addClass('is-hidden') ;
                } else {
                    ( label.offset().left > resizeElement.offset().left + resizeElement.outerWidth() ) ? label.removeClass('is-hidden') : label.addClass('is-hidden') ;
                }
            }
        },
        init: function () {
            // Desktop and mobile
            this.initCompareImages();
            this.updateCompareImages();
            return this
        }
    }
    window.colorBlindQuiz = colorBlindQuiz
})();

$(window).on('load', function(){        

    colorBlindQuiz.init();

    if (/^((?!chrome|android).)*safari/i.test(navigator.userAgent)) {
        $(".compare_images").css('height', 676);
    }
    
}) /* window load */  

$(window).resize(function() {
    if ($(window).width() <= 991) {
        $(".compare_image_color").css('width', $(window).width());
        console.log($(window).width());
    }
    else {
        $(".compare_image_color").css('width', 675);
    }
}).resize();